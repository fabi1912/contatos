package br.itau.treinamento.contato.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import br.itau.treinamento.contato.model.Contato;

public interface ContatoRepository extends CrudRepository<Contato, Long> {
	
	
	String QUERY_BUSCA_CONTATO_BY_USUARIO ="SELECT * from contato"
			+ " WHERE usuario_id = :usuarioId";

	@Query(value=QUERY_BUSCA_CONTATO_BY_USUARIO,nativeQuery = true)
	List<Contato> buscaContato(Integer usuarioId);

}
