package br.itau.treinamento.contato.model.dto;

import java.util.ArrayList;
import java.util.List;

import br.itau.treinamento.contato.model.Contato;

public class ContatoMapper {
	
	
	public static Contato toContato(ContatoRequest contatoRequest, Integer usuarioId) {
		Contato contato = new Contato();
		contato.setNome(contatoRequest.getNome());
		contato.setTelefone(contatoRequest.getTelefone());
		contato.setUsuarioId(usuarioId);
		return contato;
	}

	
	public static ContatoResponse toContatoResponse(Contato contato) {
		ContatoResponse contatoResponse  = new ContatoResponse ();
		contatoResponse.setNome(contato.getNome());
		contatoResponse.setTelefone(contato.getTelefone());
		return contatoResponse;
	}
	
	public static List<ContatoResponse> toListContatoResponse(List<Contato> listContato){
		List<ContatoResponse> listaContatoResponse = new ArrayList<ContatoResponse>();
		
		listContato.forEach(n -> listaContatoResponse.add(
				new ContatoResponse(n.getNome(),n.getTelefone())));
		
		return listaContatoResponse;	
	}
}
