package br.itau.treinamento.contato.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.itau.treinamento.contato.model.Contato;
import br.itau.treinamento.contato.repository.ContatoRepository;

@Service
public class ContatoService {
	
	@Autowired
	private ContatoRepository contatoRepository;
	
	public Contato salvaContato(Contato contato) {
		return contatoRepository.save(contato);
	}
		
	public List<Contato> buscaContato (Integer usuarioId) {
		return contatoRepository.buscaContato(usuarioId);
	}

}
