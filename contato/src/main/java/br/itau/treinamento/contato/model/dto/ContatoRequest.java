package br.itau.treinamento.contato.model.dto;


public class ContatoRequest {
	
	private String nome;
	private Long telefone;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Long getTelefone() {
		return telefone;
	}
	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}
	
	

}
