package br.itau.treinamento.contato.model.dto;

public class ContatoResponse {

	private String nome;
	private Long telefone;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public Long getTelefone() {
		return telefone;
	}
	public void setTelefone(Long telefone) {
		this.telefone = telefone;
	}
	public ContatoResponse(String nome, Long telefone) {
		super();
		this.nome = nome;
		this.telefone = telefone;
	}
	public ContatoResponse() {
		super();
		
	}
	
	
	
}
