package br.itau.treinamento.contato.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.contato.model.dto.ContatoMapper;
import br.itau.treinamento.contato.model.dto.ContatoRequest;
import br.itau.treinamento.contato.model.dto.ContatoResponse;
import br.itau.treinamento.contato.security.Usuario;
import br.itau.treinamento.contato.service.ContatoService;

@RestController
public class ContatoController {
	
    Logger logger = LoggerFactory.getLogger(ContatoController.class);

    @Autowired
    private ContatoService contatoService;
	
	@PostMapping("/contato")
    @ResponseStatus(code = HttpStatus.CREATED)
	public ContatoResponse  salvaContato(@Valid @RequestBody ContatoRequest contatoRequest ,
			@AuthenticationPrincipal Usuario usuario) {
		
		logger.info("Salvando contato " + contatoRequest.getNome() );
		
		return ContatoMapper.toContatoResponse(
				contatoService.salvaContato(
						ContatoMapper.toContato(contatoRequest, usuario.getId())));
		
		
	}
	
	@GetMapping("/contatos")
	public List<ContatoResponse> buscaListaContato(@AuthenticationPrincipal Usuario usuario){
		
		logger.info("Buscando lista de contato do usuario : " + usuario.getName());

		return ContatoMapper.toListContatoResponse(contatoService.buscaContato(usuario.getId()));
	}
	
}
