package br.itau.treinamento.usuario.controller;

import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.itau.treinamento.usuario.security.Usuario;

@RestController
public class ContatoController {
	
	@GetMapping("/info")

	public Usuario exibeUsuario(@AuthenticationPrincipal Usuario usuario) {
		return usuario;	
	}

}
